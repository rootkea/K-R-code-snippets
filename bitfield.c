#include <stdio.h>

struct foo
{
	int ;									// Does NOT declare anything
	unsigned int bit : 1;							// (un)signed is implementation-dependent so explicit
	signed char b;								// (un)signed is implementation-dependent so explicit
	unsigned int : 1;							// Unnamed 1 bit padding
	int n;
	signed char d;
	unsigned int : 0;							// padding till next word boundry 
	signed char c;
//	float f : 1;								// Error : only int type allowed
//	unsigned int bitarray[3] : 3;						// Error : array of bit-fields NOT allowed
};

union bar
{
	unsigned int bitu : 1;							// bit-fields can be declared only in struct & union
	char a;
};

int main(int argc, char **argv)
{
/*****************************************************************************/
	printf("sizeof(struct foo) = %lu\n", sizeof(struct foo));
	struct foo fvar;
//	printf("&fvar.bit = %p\n", &fvar.bit);					// Error : Can not take address of bit-field
//	printf("sizeof(fvar.bit) = %lu\n", sizeof(fvar.bit));			// Error : Can NOT apply sizeof operator to bit-field
/*****************************************************************************/
	printf("&fvar = %p\n", &fvar);
	printf("&fvar.b = %p\n", &fvar.b);
	printf("&fvar.n = %p\n", &fvar.n);
	printf("&fvar.d = %p\n", &fvar.d);
	printf("&fvar.c = %p\n", &fvar.c);
/*****************************************************************************/
	printf("sizeof(union bar) = %lu\n", sizeof(union bar));
/*****************************************************************************/
	return 0;
}
