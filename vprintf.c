#include <stdio.h>
#include <stdarg.h>

void fun(const char * format, ...)
{
	va_list ap;

	va_start(ap, format);

	vprintf(format, ap);
/*****************************************************************************
	printf("va_arg(ap, char *) = %s\n", va_arg(ap, char *));
	vprintf("%d\n%c\n%f\n", ap);
/****************************************************************************/
	va_end(ap);

	return ;
}

int main(int argc, char **argv)
{
	fun("%s\n%d\n%c\n%f\n", "Hello", 23, 'A', 5.45);

	return 0;
}
