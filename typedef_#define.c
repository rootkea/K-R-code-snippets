/* 1) Is there any real loss in any situation in using #define instead of typedef as #define is more powerful than typedef[1]?
*Ans -	1) Loss of natural style of variable declaration
*	2) typedef obeys scope rules i.e. local to a block
*/

#include <stdio.h>

#define PTOA5(var) int * (*var)[5]

int main(int argc, char **argv)
{
	int * array[5];
	typedef int * (*PtoA) [5];

	PtoA cmp = &array;

//	int * (*)[5] a;					// Error :

	PTOA5(a);
	a = &array;

	int i;
//	typedef for(i = 0; i < 5; i++) FOR;		// [1]Error :

	return 0;
}
