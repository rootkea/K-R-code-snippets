#include <stdio.h>

#define ROWS 2
#define COLS 5
#define COLS_LESS 3
//#define ROWS_LESS 1

void fun(int (*ptr)[COLS])
{
	int i, j;

	printf("\n");
	for(i = 0; i < ROWS; i++)
	{
		for(j = 0; j < COLS; j++)
			printf("array2[%d][%d] = %d\t", i, j, ptr[i][j]);
		printf("\n");
	}

	printf("\n");
	for(i = 0; i < ROWS; i++)
	{
		for(j = 0; j < COLS; j++)
			printf("*(*(ptr + %d) + %d) = %d\t", i, j, *(*(ptr + i) + j));
		printf("\n");
	}

	printf("\n");
	for(i = 0; i < ROWS; i++)
	{
		for(j = 0; j < COLS; j++)
			printf("*(*(ptr + %d)) + %d = %d\t", i, j, *(*(ptr + i)) + j);
		printf("\n");
	}

	printf("\n");
	printf("ptr = %p\n", ptr);
	printf("*ptr = %p\n", *ptr);
	printf("**ptr = %d\n", **ptr);
	
	return ;
}

void fun1(char ptr[][COLS])
{
	int i, j;

	printf("\n");
	for(i = 0; i < ROWS; i++)
	{
		for(j = 0; j < COLS; j++)
			printf("array2[%d][%d] = %d\t", i, j, ptr[i][j]);
		printf("\n");
	}

	printf("\n");
	for(i = 0; i < ROWS; i++)
	{
		for(j = 0; j < COLS; j++)
			printf("*(*(ptr + %d) + %d) = %d\t", i, j, *(*(ptr + i) + j));
		printf("\n");
	}

	printf("\n");
	for(i = 0; i < ROWS; i++)
	{
		for(j = 0; j < COLS; j++)
			printf("*(*(ptr + %d)) + %d = %d\t", i, j, *(*(ptr + i)) + j);
		printf("\n");
	}

	printf("\n");
	printf("ptr = %p\n", ptr);
	printf("*ptr = %p\n", *ptr);
	printf("**ptr = %d\n", **ptr);
	
	return ;
}

/*
void fun2(int ptr[][COLS_LESS])
{
	int i, j;

	printf("\n");
	for(i = 0; i < ROWS; i++)
	{
		for(j = 0; j < COLS; j++)
			printf("array2[%d][%d] = %d\t", i, j, ptr[i][j]);
		printf("\n");
	}

	printf("\n");
	for(i = 0; i < ROWS; i++)
	{
		for(j = 0; j < COLS; j++)
			printf("*(*(ptr + %d)) + %d = %d\t", i, j, *(*(ptr + i)) + j);
		printf("\n");
	}

	printf("\n");
	printf("ptr = %p\n", ptr);
	printf("*ptr = %p\n", *ptr);
	printf("**ptr = %d\n", **ptr);
	
	return ;
}
*/

int main(void)
{
	int array2[ROWS][COLS] =	{
						0, 1, 2, 3, 4,
						5, 6, 7, 8, 9
					};

	fun(array2);
	fun1(array2);
//	fun2(array2);

	return 0;
}
