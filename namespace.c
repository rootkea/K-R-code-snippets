/*	Namespaces:
*	1) Labels
*	2) Tags of struct, union & enum
*	3) members of struct/union individually
*	4) Everything else
*/

#include <stdio.h>

int main(int argc, char **argv)
{
	int foo;					// 4)

	struct foo					// 2)
	{
		int foo;				// 3)
	};

//	typedef char foo;				// 4) Error : 'foo' redeclared as different kind of symbol

	foo:						// 1)

	return 0;
}
