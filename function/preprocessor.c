#define hello hello world
#define print(exp) printf(#exp)

print(Hello World!);
print("Hello, World!");

//print(Hello, World);						// 2 arguments
print(Hello(,) World);

print(Hello"\n" World!);
print(Hello\n"" World!);
print(Hello\\n World!);

print(hello);
