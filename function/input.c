/*
 * input.c
 * This file is part of Sum
 *
 * Copyright (C) 2015 - Avinash Sonawane
 *
 * Sum is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Sum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Sum. If not, see <http://www.gnu.org/licenses/>.
 */

void input(int *a, int *b)
{
	printf("1st No. = ");
	scanf("%d", a);
	printf("2nd No. = ");
	scanf("%d", b);

	return ;
}
