#include <stdio.h>
#define avi 123
//#include "sum.h"

//static extern int n;				// Error : multiple storage classes
extern int n;
int m = 500;
int r = 200;
static stat = 20;

int foobar(void);
int sum(void);

float foo(void)
{
	printf("In foo!\n");
	bar();					// Implicit declaration	
	return 3.12;
}

float foo(void);

int main(void)
{
//	extern int n;
	
	int m = 12;
	printf("Hello\"\\n\" World!");
	
	printf("main: m = %d\n", m);
	{
		extern int m;
		static int avinash = 100;

		printf("block in main: m = %d\n", m);
		printf("block in main: avinash = %d\n", avinash);
		printf("block in main: stat = %d\n", stat);
	}

	avi;
	printf("main: m = %d\n", m);
	printf("return value of foo = %f\n", foo());
	foobar();
	sum();

	return 0;
}


int bar(void)
{
//	extern int n;

	printf("In bar! n = %d\n", n);
	return 0;
}

int foobar(void)
{
	printf("In foobar!\n");
	bar();					// Implicit declaration
	return 0;
}
