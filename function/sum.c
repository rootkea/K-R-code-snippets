/*
 * sum.c
 * This file is part of Sum
 *
 * Copyright (C) 2015 - Avinash Sonawane
 *
 * Sum is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Sum is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Sum. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
//#include "sum.h"

int n;

//register int a = 10;				// Error :

//extern int m;					// Error : static declaration after non-static declaration
static int m;
//int m = 100;					// Error : non-static declaration after static declaration
extern int m;

//static int sum(void);				// Error : Linker error
static int m;
int n;

int sum(void)
{
//	static int r;				// Error
//	extern int r;				// Error
	register int r = 10;
//	extern int r;				// *Error :
//	static int r;				// *Error :

//	printf("sum: &r = %p\n", &r);		// Error : Address of register variable
	printf("sum: n = %d\n", n);
	printf("sum: m = %d\n", m);
//	avi;					// Error : undeclared

	return 4.56;
}

//int n = 44;
//static int m = 200;
int n;
