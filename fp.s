	.file	"fp.c"
	.section	.rodata
.LC0:
	.string	"In fun!"
.LC1:
	.string	"Avinash"
	.text
	.globl	fun
	.type	fun, @function
fun:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$.LC0, %edi
	call	puts
	movl	$.LC1, %edi
	call	puts
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	fun, .-fun
	.section	.rodata
.LC2:
	.string	"In function 1! a = %d\n"
	.text
	.globl	fun1
	.type	fun1, @function
fun1:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC2, %edi
	movl	$0, %eax
	call	printf
	nop
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE1:
	.size	fun1, .-fun1
	.section	.rodata
.LC3:
	.string	"In function 2!"
	.text
	.globl	fun2
	.type	fun2, @function
fun2:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movl	$.LC3, %edi
	call	puts
	nop
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	fun2, .-fun2
	.section	.rodata
	.align 8
.LC4:
	.string	"fun = %p\t &fun = %p\t *fun = %p\t **fun = %d\n"
	.align 8
.LC5:
	.string	"fun1 = %p\t &fun1 = %p\t *fun1 = %p\t **fun1 = %d\n"
	.align 8
.LC6:
	.string	"main = %p\t &main = %p\t *main = %p\t **main = %d\n"
	.align 8
.LC7:
	.string	"fp = %p\t &fp = %p\t *fp = %p\t **fp = %d\n"
.LC8:
	.string	"fp = %p\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$32, %rsp
	movl	%edi, -20(%rbp)
	movq	%rsi, -32(%rbp)
	movl	$15, -12(%rbp)
	movl	$fun, %r8d
	movl	$fun, %ecx
	movl	$fun, %edx
	movl	$fun, %esi
	movl	$.LC4, %edi
	movl	$0, %eax
	call	printf
	movl	$fun1, %r8d
	movl	$fun1, %ecx
	movl	$fun1, %edx
	movl	$fun1, %esi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	movl	$main, %r8d
	movl	$main, %ecx
	movl	$main, %edx
	movl	$main, %esi
	movl	$.LC6, %edi
	movl	$0, %eax
	call	printf
	movq	$fun, -8(%rbp)
	movq	-8(%rbp), %rsi
	movq	-8(%rbp), %rcx
	movq	-8(%rbp), %rax
	leaq	-8(%rbp), %rdx
	movq	%rsi, %r8
	movq	%rax, %rsi
	movl	$.LC7, %edi
	movl	$0, %eax
	call	printf
	movq	-8(%rbp), %rax
	call	*%rax
	movq	-8(%rbp), %rax
	call	*%rax
	movq	$fun1, -8(%rbp)
	movq	$fun2, -8(%rbp)
	movq	-8(%rbp), %rax
	call	*%rax
	movq	$fun, -8(%rbp)
	movq	-8(%rbp), %rax
	addq	$1, %rax
	movq	%rax, -8(%rbp)
	movq	-8(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC8, %edi
	movl	$0, %eax
	call	printf
	movq	-8(%rbp), %rax
	call	*%rax
	movl	$0, %eax
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.8.1-10ubuntu9) 4.8.1"
	.section	.note.GNU-stack,"",@progbits
