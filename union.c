#include <stdio.h>

union foo
{
	int a;
	double b;
};

int main(int argc, char **argv)
{
/*****************************************************************************/
	union foo fvar = {20.67};
//	union foo fvar = {21.67, 11, 23.45};
//	union foo fvar = {{23.67, 11}, 23.45};
//	union foo fvar = {22.67, {11, 23.45}};					// Error : extra brace group at the end of initializer

	printf("fvar.a = %d\n", fvar.a);
	printf("fvar.b = %f\n", fvar.b);
/*****************************************************************************
	fvar.a = 10;
	printf("fvar.a = %d\n", fvar.a);
	printf("fvar.b = %f\n", fvar.b);
/*****************************************************************************
	fvar.b = 3.14;
	printf("fvar.a = %d\n", fvar.a);
	printf("fvar.b = %f\n", fvar.b);
/*****************************************************************************/
	printf("sizeof(union foo) = %lu\n", sizeof(union foo));
	printf("&fvar = %p\n", &fvar);
	printf("&fvar.a = %p\n", &fvar.a);
	printf("&fvar.b = %p\n", &fvar.b);
/*****************************************************************************/

	return 0;
}
