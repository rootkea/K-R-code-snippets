#include <stdio.h>
#include <stdlib.h>

void foo(void)
{
	printf("In foo!\n");
	return ;
}

void bar(void)
{
	printf("In bar!\n");
	return ;	
} 

int main(int argc, char **argv)
{
	char str[] = "4.567avinash";
	char *ptr;
/*****************************************************************************
	printf("strtod(str, &ptr) = %f\n", strtod(str, &ptr));
	puts(ptr);
/*****************************************************************************
	if(atexit(foo))
		printf("Could not register foo() function\n");
	if(atexit(bar))
		printf("Could not register bar() function\n");
/*****************************************************************************
	printf("Hello %s!\n", getenv("USER"));
/*****************************************************************************/
	div_t q = div(17, 5);
	printf("q.quot = %d\n", q.quot);
	printf("q.rem = %d\n", q.rem);
/*****************************************************************************/

	return 0;
}
