#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	FILE *fp;
	int n;

//	remove("foo.txt");
//	rename("foo.txt", "foo1.txt");
	printf("He%nllo!\n", &n);
	printf("n = %d\n", n);
/*****************************************************************************	
	printf("tmpnam(NULL) = %s\n", tmpnam(NULL));
//	freopen("foo.txt", "w", fp);						// Segmentation fault. freopen = fclose then fopen
/*****************************************************************************
	int i;
	for(i = 1; i <= 1022; i++)
	{
		fp = fopen("file.txt", "w");
		printf("%4d) fp = %p\n", i, fp);
//		fclose(fp);
	}
/*****************************************************************************
	fp = fopen("file.txt", "w");
	fprintf(fp,"A\n");

//	fclose(fp);
	freopen("foo.txt", "w", fp);
	fprintf(fp,"B\n");
/*****************************************************************************
	FILE *fp1 = malloc(sizeof(FILE));
	*fp1 = *fp;
//	fclose(fp);
//	scanf("%*d");								// Examine foo.txt at this instance
//	fprintf(fp1,"C\n");							// Segmentation Fault
/******************************************************************************/
	return 0;
}
