#include <stdio.h>

int main(int argc, char **argv)
{
	FILE *fp;
	fpos_t *ptr;

	fp = fopen("foo.txt", "w+");
	
	fputs("Hello World!\n", fp);
	printf("ftell(fp) = %ld\n", ftell(fp));

	fseek(fp, ftell(fp) - 6, SEEK_SET);
	printf("getc(fp) = %c\n", getc(fp));

	fseek(fp, 1, SEEK_CUR);
	printf("getc(fp) = %c\n", getc(fp));

	fseek(fp, -6, SEEK_END);
	printf("getc(fp) = %c\n", getc(fp));

	fgetpos(fp, ptr);
	printf("ptr = %ld\n", ptr);

	fclose(fp);

	return 0;
}
