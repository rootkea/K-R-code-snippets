#include <stdio.h>
#include <stdarg.h>

void varfun(int, ...);

int main(int argc, char **argv)
{
	int val, dummy = 1;
	char c; float f;
//	varfun(dummy, 24, 5.67, "Avinash");
	
	printf("In main!\n");
	
	varfun(dummy, 4, 'r', "PICT");

	val = scanf("%f%c",&f, &c);
	printf("val = %d\n", val);
	printf("f = %f\n", f);
	printf("c = %c\n", c);

	val = scanf("%c%f",&c, &f);
	printf("val = %d\n", val);
	printf("c = %c\n", c);
	printf("f = %f\n", f);
	

	return 0;
}

void varfun(int n, ...)
{
	va_list ap;

	va_start(ap, n);

		printf("va_arg(ap, int) = %d\n", va_arg(ap, int));
		printf("va_arg(ap, int) = %c\n", va_arg(ap, int));
//		printf("va_arg(ap, double) = %f\n", va_arg(ap, double));
		printf("va_arg(ap, char *) = %s\n", va_arg(ap, char *));

	va_end(ap);

	va_start(ap, n);

		printf("va_arg(ap, int) = %d\n", va_arg(ap, int));
		printf("va_arg(ap, int) = %c\n", va_arg(ap, int));
//		printf("va_arg(ap, double) = %f\n", va_arg(ap, double));
		printf("va_arg(ap, char *) = %s\n", va_arg(ap, char *));

	va_end(ap);

	return ;
}
