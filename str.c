#include <stdio.h>
#include <string.h>

int main(int argc, char **argv)
{
	char str[] = "Hello World! Avinash Sonawane OOps!";
//	char *str = "Hello World! Avinash Sonawane OOps!";			// causes segmenation fault in Line NO. 17
	char *delim = " ";
	char *token;
	int i , size = strlen(str);
/*****************************************************************************
//	strcpy(str, str);							// Undefined beehavior
	printf("strerror(0) = %s\n", strerror(0));
/*****************************************************************************/
	printf("str = %s\n", str);
	
	printf("strtok(str, delim) = %s\n", strtok(str, delim));
	while((token = strtok(NULL, delim)))
		printf("token = %s\n", token);

	printf("str = %s\n", str);

	for(i = size-1; i >= 0; i--)
	{
		if(!str[i])
			str[i] = ' ';
	}
	
	printf("str = %s\n", str);

	return 0;
}
