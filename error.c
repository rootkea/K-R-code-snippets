#include <stdio.h>
#include <errno.h>

int main(int argc, char **argv)
{
	FILE *fp;
	
	fp = fopen("file.txt", "w+");

	fputc('A', fp);
	fseek(fp, 0, SEEK_SET);

	printf("fgetc(fp) = %c\n", fgetc(fp));
	printf("feof(fp) = %d\n", feof(fp));

	fopen("avi.txt", "r");
	perror("fopen");
	fgetc(fp);
	printf("feof(fp) = %d\n", feof(fp));

	fclose(fp);
	
	return 0;
}
