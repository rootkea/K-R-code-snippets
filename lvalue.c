#include <stdio.h>

int main(int argc, char **argv)
{
	int a = 3, b, *ptr = &a;

//	(1 ? a : b) = 5;			// Error : lvalue required
//	(a, b) = 5;				// Error : lvalue required

//	3 = 5;					// Error : lvalue required
	*ptr = 5;

	printf("*ptr = %d\n", *ptr);

	return 0;
}
