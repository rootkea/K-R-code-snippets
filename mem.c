#include <stdio.h>
#include <string.h>

#define SIZE 5

int main(int argc, char **argv)
{
	char sc[25] = "Hello World!";
	char dc[25];
	int si[SIZE] = {1, 2, 3, 4, 5};
	int di[SIZE]; int i;

	memcpy(dc, sc, 3);
	printf("dc = %s\n", dc);

	memcpy(di, si, 17);
	for(i = 0; i < SIZE; i++)
		printf("di[%d] = %d\n", i, di[i]);

	return 0;
}
