#include <stdio.h>

#define PLANES	3
#define ROWS	2
#define COLS	5

int main(void)
{
	char c;
	int i, j, k;
	
	int array1[COLS] = 			{0, 1, 2, 3, 4, {5, 6}};

	int array2[ROWS][COLS] = 		{
							0, 1, 2, 3, 4,
							5, 6, 7, 8, 9
						};

	int array3[PLANES][ROWS][COLS] = 	{
							{
								0, 1, 2, 3, 4,
								5, 6, 7, 8, 9
							},
					
							{
								10, 11, 12, 13, 14,
								15, 16, 17, 18, 19
							},
					
							{
								20, 21, 22, 23, 24,
								25, 26, 27, 28, 29
							}
						};

	printf("\n");
	printf("array1[5]\n\n");
	for(i = 0; i < COLS; i++)
		printf("array1[%d] = %d\t", i, array1[i]);
	printf("\n");
printf("****************************************************************************************************************************\n");
	printf("Enter 'y' to continue; 'n' to exit.\n: ");
	while((c = getchar())!='y' && c != 'n')
		;
	if(c == 'n')
		goto exit;

	printf("\n");
	printf("array2[2][5]\n\n");
	for(i = 0; i < ROWS; i++)
		printf("array2[%d] = %p\t", i, array2[i]);
	printf("\n\n");

	for(i = 0; i < ROWS; i++)
		printf("*array2[%d] = %d\t", i, *array2[i]);
	printf("\n\n");

	for(i = 0; i < ROWS; i++)
		{
			for(j = 0; j < COLS; j++)
				printf("array2[%d][%d] = %d\t", i, j, array2[i][j]);
			printf("\n");
		}
	printf("\n");
printf("****************************************************************************************************************************\n");
	printf("Enter 'y' to continue; 'n' to exit.\n: ");
	while((c = getchar())!='y' && c != 'n')
		;
	if(c == 'n')
		goto exit;

	printf("\n");
	printf("array3[3][2][5]\n\n");
	for(i = 0; i < PLANES; i++)
		printf("array3[%d] = %p\t", i, array3[i]);	
	printf("\n\n");
	
	for(i = 0; i < PLANES; i++)
		printf("*array3[%d] = %p\t", i, *array3[i]);	
	printf("\n\n");

	for(i = 0; i < PLANES; i++)
		printf("**array3[%d] = %d\t", i, **array3[i]);	
	printf("\n\n");

	for(i = 0; i < PLANES; i++)
	{
		for(j = 0; j < ROWS; j++)
			printf("array3[%d][%d] = %p\t", i, j, array3[i][j]);
		printf("\n");
	}
	printf("\n");
	
	for(i = 0; i < PLANES; i++)
	{
		for(j = 0; j < ROWS; j++)
			printf("*array3[%d][%d] = %d\t", i, j, *array3[i][j]);
		printf("\n");
	}
//	printf("\n");
	
	for(i = 0; i < PLANES; i++)
	{
		printf("\n\n");
		for(j = 0; j < ROWS; j++)
		{
			for(k = 0; k < COLS; k++)
				printf("array3[%d][%d][%d] = %d\t", i, j, k, array3[i][j][k]);
			printf("\n");
		}
	}
	printf("\n");

	exit :
	return 0;
}
