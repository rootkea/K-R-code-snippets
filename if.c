/*
*	Written By : Avinash Sonawane
*	Date : 
*	
*/

#include <stdio.h>
#include <stdlib.h>

void foo(void);

int main(void)
{
	char a = -128;

	printf("a = %d\n", a);
	
//	break;					//Error Not within Loop or switch
//	continue;				//Error Not within Loop

	a = a * -1;
	printf("a = %d\n", a);

	printf("abs(-2147483648) = %d\n", abs(-2147483648));

	a = 128;
	printf("a = %d\n", a);	

	foo();

/*
	if(getchar();)
		printf("a = 5\n");


	while()					//Error expression missing
		;
	
	do
		;
	while();				//Error expression missing
	

	for( ; ; )				// Infinite Loop
		printf("Hello World!\n");
*/

	end:

	return 0;
}

void foo(void)
{
	printf("In foo\n");

//	goto end;				//Error Label not found

	return ;
}
