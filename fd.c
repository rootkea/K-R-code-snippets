#include <stdio.h>

int main(int argc, char **argv)
{
	FILE *fp;

	printf("stdin->_fileno = %d\n", stdin->_fileno);
	printf("stdout->_fileno = %d\n", stdout->_fileno);
	printf("stderr->_fileno = %d\n", stderr->_fileno);

	fp = fopen("file.txt", "w+");

	printf("fp->_fileno = %d\n", fp->_fileno);
	stdin = fp;
	printf("stdin->_fileno = %d\n", stdin->_fileno);

	fclose(fp);

	return 0;
}
