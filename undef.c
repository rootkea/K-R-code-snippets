#include <stdio.h>

#define size 1

int main(int argc, char **argv)
{
	int i;
	for(i = 0; i < size; i++)
		printf("Hello World!\n");

	#undef size

	size();

	return 0;
}

int size(void)
{
	printf("In size!\n");
	return 0;
}
