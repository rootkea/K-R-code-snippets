/*
 * duff.c
 * This file is part of <program name>
 *
 * Copyright (C) 2015 - Avinash Sonawane
 *
 * <program name> is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * <program name> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int main(void)
{
	int n, count, i;

	printf("n = ");
	scanf("%d", &n);

	count = 0;
	i = n / 4;
	switch(n % 4)
	{
		case 0: while(i-- > 0) {printf("%d) Hello!\n", ++count);

		case 3: 		printf("%d) Hello!\n", ++count);

		case 2: 		printf("%d) Hello!\n", ++count);

		case 1: 		printf("%d) Hello!\n", ++count);
					}
	}
/*
	count = 0;
	i = n / 4;
	switch(n % 4)
	{
		case 0:
		while(i-- > 0)					//Outer switch can enter into any inner block except another switch block
		{
			printf("%d) Hello!\n", ++count);

			case 3:
			printf("%d) Hello!\n", ++count);

			case 2:
			printf("%d) Hello!\n", ++count);

			case 1:
			printf("%d) Hello!\n", ++count);
		}
	}
*/

	i = 10;
	n = 20;
	switch(i)
	{
		
		case 10:
		printf("i == 10\n");
		switch(n)				//Outer switch can't enter into inner switch block
		{
			case 15:
			printf("n == 15\n");

			case 20:
			printf("n == 20\n");
			
			case 10:			// Case label of Inner switch
			printf("n == 10\n");		// Has nothing to do with outer switch
		}

		case 5:
		printf("i == 5\n");

		case 15:
		printf("i == 15\n");
	}

	return 0;
}
