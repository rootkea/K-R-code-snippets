#include <stdio.h>

int main(int argc, char **argv)
{
	FILE *fp;

	fp = fopen("file.txt", "w");

	fputs("Hello World!\n", fp);
	fseek(fp, 0, SEEK_SET);
	printf("getc(fp) = %d\n", getc(fp));					// EOF : No read permission

	fclose(fp);

	return 0;
}
