#include <stdio.h>

#define SIZE 10

int main(int argc, char **argv)
{
	typedef int a[SIZE];
	int i;

	a b;

	for(i = 0; i < SIZE; i++)
		b[i] = i;

	for(i = 0; i < SIZE; i++)
		printf("b[%d] = %d\n", i, b[i]);

	printf("sizeof(b) = %lu\n", sizeof(b));

	return 0;
}
