#include <stdio.h>

int main(int argc, char **argv)
{
	int a = 5, *ptr;
//	void temp;			// Error : variable declared 'void'

	void *ap = &a;
	ptr = ap;
	
	printf("*ptr = %d\n", *ptr);

	return 0;
}
