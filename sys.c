#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#define SIZE 5

int main(int argc, char **argv)
{
	char buf[SIZE];
	int n, fd;
	char c;
/*****************************************************************************
	c = fgetc(stdin);							// I/O stream buffers are flushed by fclose at the end
	putc(c, stdout);
/*****************************************************************************
	printf("n = read(0, buf, SIZE) = %d\n", n = read(0, buf, SIZE));	// I/O buffers are NOT flushed automatically

/*	printf("n = read(5, buf, SIZE) = %d\n", n = read(5, buf, SIZE)); */
/*	perror("read"); */

/*	if(feof(stdin))								// Does NOT work for read/write
		printf("EOF!\n");	*/
/*****************************************************************************
	while((n = read(0, buf, SIZE)) > 0)
		write(1, buf, n);
/*****************************************************************************/
	fd = open("foo.txt", O_RDWR, 0777);
	if(fd == -1)
		perror("open");
/*	
	fd = creat("foo.txt", 0777);
	if(fd == -1)
		perror("creat");
*/
	unlink("foo.txt");

	if(close(fd) == -1)
		perror("close");

	return 0;
}
