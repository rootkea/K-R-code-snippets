#include <stdio.h>
#include <math.h>

int main(int argc, char **argv)
{
	double n, *ip = &n;

	printf("modf(4.37, ip) = %f\n", modf(4.37, ip));
	printf("n = %d\n", (int)n);

	return 0;
}
