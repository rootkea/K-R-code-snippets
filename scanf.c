#include <stdio.h>

int main(int argc, char **argv)
{
	char s[25];
	char c, *ptr;
/*****************************************************************************
	scanf("%10c", s); s[10]='\0';
	puts(s);

	scanf("%24s", s);							// %s must always have width
	puts(s);
/*****************************************************************************
	scanf("%p", &ptr);
	printf("%p\n", ptr);
/*****************************************************************************
	scanf("%24[][a-zA-Z]", s);						// %[] must always have width
	puts(s);

	scanf("%24[^a-zA-Z]", s);						// %[^] must always have width
	puts(s);
/*****************************************************************************/
	scanf("%4s", s);							// string with white-space chars - NOT possible with scanf
	printf("%s\n", s);

	scanf(" %c", &c);							// Accept next non-white space character 
	printf("%c\n", c);
/*****************************************************************************/
	return 0;
}
