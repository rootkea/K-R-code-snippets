/*
*	Written By : Avinash Sonawane
*	Date : 
*	
*/

#include <stdio.h>

int foo(int, int *);

int main(void)
{
	int n = 10;
/*
	n++ , printf("n = %d\n", n) , ++n , printf("n = %d\n", n);	//op precedence "++ more than ," then why , op left-to-right eval?

	++n , printf("n = %d\n", n) , n++ , printf("n = %d\n", n);
*/

	n = (n = 7) + 10;						// Is this undefined?
	printf("n = %d\n", n);

	foo(n++, &n) + printf("Hello World!\n");

	return 0;
}


int foo(int a, int *ptr)
{
	printf("a = %d\n", a);
	printf("*ptr = %d\n", *ptr);
	return 0;
}
