/*
 * goto.c
 * This file is part of <program name>
 *
 * Copyright (C) 2015 - Avinash Sonawane
 *
 * <program name> is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * <program name> is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with <program name>. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int main(void)
{
	int i = 10;
/*********************************************************

	goto duff;
	for(i = 5; i > 0; i--)
	duff:
	{
		printf("i = %d\n", i);
//		duff:
		printf("Hello!\n");
	}
/**********************************************************/
	
	i=10; 
	goto foo;
	if (i==5)
	foo:
	{
		printf("i = 5\n");
//		foo:							//Error label at the end of compound statement
	}
//	foo:								//Error else without if
	else
		printf("i = %d\n", i);
/*********************************************************

	goto bar;
	switch(i)
	bar:
	{
		case 5:
		printf("i == 5\n");
		
		case 10:
		printf("i == 10\n");
		break;
		
		default:
		printf("Default\n");
	}
/*********************************************************/

	return 0;
}
