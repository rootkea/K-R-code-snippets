#include <stdio.h>

struct foo
{
	int n;
};

int main(int argc, char **argv)
{
	struct foo foovar1 = {5}, foovar2 = {10};
	struct foo *fooptr1 = &foovar1, *fooptr2 = &foovar2;

	(fooptr1 = fooptr2) -> n = 15;
//	(*fooptr1 = *fooptr2) . n = 15;

	printf("foovar1.n = %d\n", foovar1.n);
	printf("foovar2.n = %d\n", foovar2.n);

//	(foovar1 = foovar2) . n = 15;						// Error : lvalue required
	printf("(foovar1 = foovar2).n = %d\n", (foovar1 = foovar2).n);
	printf("foovar1.n = %d\n", foovar1.n);
	printf("foovar2.n = %d\n", foovar2.n);

	return 0;
}
