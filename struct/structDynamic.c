#include <stdio.h>
#include <stdlib.h>

struct list
{
	int n;
	struct list *next;
};

int main(int argc, char **argv)
{
	struct list *p, *root, *last;
	int i, size;
	
	printf("Total Nodes : ");
	scanf("%d", &size);
	for(i = 0; i < size; i++)
	{
		if(p = malloc(sizeof(struct list)))
		{
			printf("n = ");
			scanf("%d", &(p->n));
			p->next = NULL;
		
			if(!i)
				root = p;
			else
				last->next = p;

			last = p;
		}
		else
		{
			printf("NOT enough memory!\n");
			return 1;
		}	

	}

	for(p = root; p; p = p->next)
		printf("%d\n", p->n);

	return 0;
}
