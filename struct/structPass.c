#include <stdio.h>

void bar(struct foo *f)						// struct local to bar() // argument & passed value mismatch
{
	printf("f->a = %d\n", f->a);				// Error: dereferencing pointer to incomplete type
	return ;
}

int main(int argc, char **argv)
{
	struct foo						// struct local to main()
	{
		int a;
	};

	struct foo fvar, *fptr = &fvar;
	fvar.a = 10;
	bar(fptr);
	return 0;
}
