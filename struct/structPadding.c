#include <stdio.h>

struct point
{
	int x;
	int y;
};

struct rect
{
	int a;
	char b;
	struct point *pt;
};

int main(int argc, char **argv)
{
	struct rect r1;

/******************************************************************************************/
	printf("sizeof(struct rect) = %lu\n", sizeof(struct rect));

	printf("&r1 = %p\n", &r1);
	printf("&r1.a = %p\n", &r1.a);
	printf("&r1.b = %p\n", &r1.b);
	printf("&r1.pt = %p\n", &r1.pt);

	return 0;
}
