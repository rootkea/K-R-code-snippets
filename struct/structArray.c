#include <stdio.h>

#define SIZE 10

struct point
{
	int x;
	int y;
};

int main(int argc, char **argv)
{
	int i;
	struct point points[SIZE] = 	{
						{1, 2},
						3, 4,
						{5, 6, 7},
						{8},
						9, 10,
						11, 12,
						13, {14, 15},
						16, 17,
						{18, 19},
						{20, 21}
					};

	for(i = 0; i < SIZE; i++)
		printf("points[%d].x = %d \tpoints[%d].y = %d\n", i, points[i].x, i, points[i].y);

	return 0;
}
