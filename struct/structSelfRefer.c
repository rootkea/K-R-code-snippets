#include <stdio.h>

struct rect
{
	int a;
	struct rect *pt;
};

int main(int argc, char **argv)
{
	struct rect r1, *pt;

	pt = &r1;
	pt->pt = &r1;

	pt->pt->pt->pt->pt->a = 3;
	(*(*(*(*(*pt).pt).pt).pt).pt).a = 3;

	return 0;
}
