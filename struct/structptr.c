#include <stdio.h>

struct rect
{
	int a;
	char b;
	int x, y;
};

int main(int argc, char **argv)
{
	struct rect r1, r2, *rp;

	printf("sizeof(struct rect) = %lu\n", sizeof(struct rect));
	printf("&r1 = %p\n", &r1);
	printf("&r2 = %p\n", &r2);

	rp = &r1;
	printf("rp++ = %p\n", rp++);
	printf("rp = %p\n", rp);

	return 0;
}
