/* 1) structure_tag, member & non-member */

#include <stdio.h>

struct foobar
{
	int a;
	int b;
};

struct point
{
	int x;
	int y;
	struct foobar z;
};

struct foo
{
	int x;
	int foo;
/*	int z = 20;			// Error : Definition inside struct not allowed	*/
	struct point pt;
	struct point pt1;
	struct nest			// External structure
	{
		int nesta;
		int nestb;
	} nestvar;
} foo, foo;				/* Tentative Definition */

int main(int argc, char **argv)
{
	int a, b;
/**************************************************************************************************************************
	printf("Enter a and b :");
	scanf("%d%d", &a, &b);
	struct foo sname = {++a, ++b, {++a},{a-10, b-20}};

	printf("sname.x = %d\tsname.foo = %d\n", sname.x, sname.foo);
	printf("sname.pt.x = %d\tsname.pt.y = %d\n", sname.pt.x, sname.pt.y);
	printf("sname.nestvar.nesta = %d\tsname.nestvar.nestb = %d\n", sname.nestvar.nesta, sname.nestvar.nestb);
/**************************************************************************************************************************
	struct nest var = {10, 20};						// struct nest is external structure
	printf("var.nesta = %d\tvar.nestb = %d\n", var.nesta, var.nestb);
/*************************************************************************************************************************
	struct foo foovar = 	{
					1, {2, 3}, 
					{4, 5},
					6, 7,{8, 9, 10},
					{6, 7}
				};

	printf("foovar.x = %d\tfoovar.foo = %d\n", foovar.x, foovar.foo);
	printf("foovar.pt.x = %d\tfoovar.pt.y = %d\n", foovar.pt.x, foovar.pt.y);
	printf("foovar.pt.z.a = %d\tfoovar.pt.z.b = %d\n", foovar.pt.z.a, foovar.pt.z.b);
	printf("foovar.pt1.z.a = %d\tfoovar.pt1.z.b = %d\n", foovar.pt1.z.a, foovar.pt1.z.b);
	printf("foovar.nestvar.nesta = %d\tfoovar.nestvar.nestb = %d\n", foovar.nestvar.nesta, foovar.nestvar.nestb);
/**************************************************************************************************************************/
//	struct foobar fbarvar1 = {1, 2, 7, {3, 4}, 5, 6};			// Error : Extra brace group at end of initializer
	struct foobar fbarvar2 = {1, 2, 3, 4, 5, 6, 7};

	return 0;
}
