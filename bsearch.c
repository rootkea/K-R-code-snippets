#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


#define SIZE 10

int cmp(const void *m, const void *n)
{
	return  (*(int *)m - *(int *)n);
}

int main(int argc, char **argv)
{
	int array[SIZE];
	int i, key;
	int *p;

	printf("Enter %d elements: ", SIZE);
	for(i = 0; i < SIZE; i++)
		scanf("%d", &array[i]);

	qsort(array, SIZE, sizeof(array[0]), cmp);
	printf("Array in sorted order: ");
	for(i = 0; i < SIZE; i++)
		printf("%d ", array[i]);
	printf("\n");

	printf("Enter key: ");
	scanf("%d", &key);

//	abort();
	assert(key<100);

	if(p = bsearch(&key, array, SIZE, sizeof(array[0]), cmp))
		printf("Key found! at location array[%ld]\n", p-array);
	else
		printf("Key NOT found!\n");

	return 0;
}
