/*
*	Written By : Avinash Sonawane
*	Date : 28/01/2015 22:47:12
*	
*/

#include <stdio.h>

int main(void)
{
	char s = 97;
	char t = 0141;
	char u = 0x61;

	char v = 'a';
	char w = '\141';
	char x = '\x61';

	printf("s = %c\n", s);
	printf("t = %c\n", t);
	printf("u = %c\n", u);
	printf("v = %c\n", v);
	printf("w = %c\n", w);
	printf("x = %c\n", x);

	printf("%s", "\0770as:" "df\n");	//1) string concatenation at compile time

	return 0;
}
