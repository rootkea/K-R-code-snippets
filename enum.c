// Enumeration constant is symbolic integer constant.
// Enum is a set of enumeration constants.

#include <stdio.h>

enum foo
{
//	int a;									// Error : Only identifiers; NO types
//	b;									// Error : Separate by Commas; No semicolon
	x, y, z = 5, t, /*u = 4.56*/ u = 5 };					// Error : Not an integer constant

enum foo * bar(enum foo *ptr)
{
	*ptr = 100;
	return ptr;
}

int main(int argc, char **argv)
{
	enum foo fvar, fvar1, *fptr;
/*****************************************************************************/
	int var = z;
	printf("var = %d\n", var);
//	z = 10;									// Error : lvalue required
//	printf("&z = %d\n", &z);						// Error : lvalue required
	printf("sizeof(z) = %lu\n", sizeof(z));					// sizeof 5 is printed and NOT of z
/****************************************************************************/
//	printf("fvar.y = %d\n", fvar.y);					// Error : fvar not a struct/union variable
	printf("fvar = %d\n", fvar);
	fvar = 20;
	printf("fvar = %d\n", fvar);
	printf("&fvar = %p\n", &fvar);
	printf("sizeof(fvar) = %lu\n", sizeof(fvar));
/****************************************************************************/
	fptr = bar(&fvar);
	fvar1 = *fptr;
	printf("fvar = %d\n", fvar);
	printf("fvar1 = %d\n", fvar1);
/****************************************************************************/
	return 0;
}
