#include <stdio.h>

int fun(void)
{
	printf("In fun!\n");
	printf("Avinash\n");
	return ;
}

int fun1(int a)
{
	printf("In function 1! a = %d\n", a);

	return ;
}

int main(int argc, char **argv)
{
	int i, n = 15;
	int array[] = { 1, 2, 3};
	int (*fp)(void);
/***********************************************************************************************************/
	printf("fun = %d\t &fun = %d\t *fun = %d\t **fun = %d\n", fun, &fun, *fun, **fun);
	printf("fun = %p\n", fun);
//	fun = fun + 1;						// Error : function name is not a lvalue
/***********************************************************************************************************/
	fp = fun;
	printf("fp = %d\t &fp = %d\t *fp = %d\t **fp = %d\n", fp, &fp, *fp, **fp);
	(*fp)();
/***********************************************************************************************************
	fun1();							// Error : Too few arguments
	fp = fun1;
	(*fp)(n);						// Error : Too many arguments
/**********************************************************************************************************/
	fp = fun;
	i = 4;
	fp = fp + i;
	printf("fp = %p\ti = %d\n", fp, i);
	(*fp)();
/**********************************************************************************************************/	
	return 0;
}
