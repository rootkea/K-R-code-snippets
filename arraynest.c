#include <stdio.h>

#define PLANES 3
#define ROWS 2
#define COLS 5

int main(int argc, char **argv)
{
	int i, j, k;
	int array3[PLANES][ROWS][COLS] = {
							{0, 1, 2, 3},
							4, 5, 6, {7, 8, 9}, 10, 
							{11, 12, 13, 14, 15, 16, 17},
							18, 19, {20, 21, 22}
					};

	for(i = 0; i < PLANES; i++)
	{
		printf("\n\n");
		for(j = 0; j < ROWS; j++)
		{
			for(k = 0; k < COLS; k++)
				printf("array3[%d][%d][%d] = %d\t", i, j, k, array3[i][j][k]);
			printf("\n");
		}
	}
	printf("\n");

	return 0;
}
