/*
 * escape.c
 * This file is part of escape
 *
 * Copyright (C) 2015 - Avinash Sonawane
 *
 * escape is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * escape is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with escape. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int main(void)
{
	
//	printf("Hello world!\n");
//	printf("\033[2J");		/* clear screen */

//	printf("\033[%d;%dH", 10, 20);	/* move cursor (row 10, col 20) */

	printf("Hello, ");
	printf("\033[7mworld\033[0m!\n");	/* inverse video */

	return 0;
}
