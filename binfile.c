#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv)
{
	FILE *fp, *fp1;
	int c = 'A';

	if(!(fp = fopen("hello", "a+")))
	{
		
		printf("Could not open hello.\n");
		exit(EXIT_FAILURE);
	}

	if(!(fp1 = fopen("hello1", "w+")))
	{
		
		printf("Could not open hello1.\n");
		exit(EXIT_FAILURE);
	}

	fseek(fp, 0, SEEK_SET);
	fseek(fp1, 0, SEEK_SET);
	while((c = fgetc(fp)) != EOF)
		fputc(c, fp1);

	fclose(fp);
	fclose(fp1);

	return 0;
}
