/*
 * switch.c
 * This file is part of switch
 *
 * Copyright (C) 2015 - Avinash Sonawane
 *
 * switch is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * switch is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with switch. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int main(void)
{
	const int i = 10;
	float f = 3.14F;

//	switch(f)				//Error Not an integeral expression
	switch(50)
	{	
		case 22:
		printf("Case 22\n");
		printf("Wow!\n");
		
		case 3:
		default:
		case 100:
		printf("Case 3\n");
/*		
		case 4.3:			//Error case label Not an integer
		;
		
		case i:				//Error case label not a constant
		;
		
		case 2:				//Error duplicate case value
		;
*/
		case 8:
		printf("In Case 8");
	}

//	goto case 8;				// Error :

	return 0;
}
