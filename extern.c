/*
 * extern.c
 * This file is part of storageClass
 *
 * Copyright (C) 2015 - Avinash Sonawane
 *
 * storageClass is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * storageClass is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with storageClass. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>

int foobar;
int *func(void);

int main(void)
{
	extern int foo;
	int n = foobar;
	int *ptr;

	ptr = func();

	printf("foo = %d\n", foo);
	printf("n = %d\n", n);

	*ptr = *ptr + 1;
	func();

	return 0;
}

int foo = 10;
//      foo = 20;

int *func(void)
{
//              static int a = foobar;
	static int a;
	a = 30;

	a++;
	printf("a = %d\n", a);

	return &a;
}
