/*
*	Written By : Avinash Sonawane
*	Date : 
*	
*/

#include <stdio.h>

int wordlength(void);

int main(void)
{
	int x, n, ans, i;
	int length = wordlength();

	printf("x = ");
	scanf("%d", &x);
	printf("n = ");
	scanf("%d", &n);

	n = n % length;
/*
		ans = x;
		for(i = 1; i <= n; i++)
			ans = ((ans & 1) << (length - 1)) | (ans >> 1);		// Solution 1
		printf("ans = %d\n", ans);
*/
	if (n)
		ans = ((~(~0 << n) & x) << (length - n)) | (x >> n);		// Solution 2
	printf("ans = %d\n", ans);

	if (n)
		ans =  (x << (length - n)) | (x >> n);				// Solution 3 (Best)
	printf("ans = %d\n", ans);

	printf("34 >> -2 = %d\n", 34 >> -2);		//Negative shifter changes the shift direction
	printf("34 << -2 = %d\n", 34 << -2);		//Negative shifter changes the shift direction

	return 0;
}

int wordlength(void)
{
	int i;
	int foo = ~0;

	for (i = 1; (foo = foo << 1) < 0; i++)
		;

	return i;
}
