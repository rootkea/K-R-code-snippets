#include <stdio.h>

//#if sizeof									// Error : preprocessor has no idea about sizeof operator

//#endif

int main(int argc, char **argv)
{
	int a = 10;
	double f = 3.14;

	printf("sizeof (f = a + f) = %lu\n", sizeof (f = a + f));		// sizeof does NOT evaluate an expression
	printf("f = %f\n", f);
	printf("sizeof (a = a + f) = %lu\n", sizeof (a = a + f));		// sizeof does NOT evaluate an expression
	printf("a = %d\n", a);

	printf("sizeof (int) = %lu\n", sizeof (int));
	printf("sizeof (5) = %lu\n", sizeof (5));
//	printf("sizeof int = %lu\n", sizeof int);				// Error: typename must be parenthesized

	return 0;
}
