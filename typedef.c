#include <stdio.h>
#include <stddef.h>

#define PTOA5(var) int * (*var)[5]

struct node
{
	int value;
	struct node *left;
	struct node *right;
};

int main(int argc, char **argv)
{
/*****************************************************/
	typedef struct node Treenode, *Treeptr;

	Treenode node1;
	Treeptr np = &node1;
/*****************************************************/
	int * array[5];
	typedef int * (*PtoA) [5];

	PtoA cmp = &array;	
/*****************************************************/
//	int * (*)[5] a;								// Error : Incorect declaration format

	PTOA5(a);
	a = &array;
/****************************************************
	int i;
	typedef for(i = 0; i < 5; i++) FOR;					// Error : unlike #define typedef works for datatypes only
/****************************************************/
	size_t var = sizeof(cmp);
	ptrdiff_t d = (array + 4) - array;

	return 0;
}
