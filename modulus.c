/*
*	Written By : Avinash Sonawane
*	Date : 
*	
*/

#include <stdio.h>

int main(void)
{
	printf("-5/2 = %d\n", -5 / 2);
	printf("-5/-2 = %d\n", -5 / -2);
	printf("-5%%2 = %d\n", -5 % 2);
	printf("-5%%-2 = %d\n", -5 % -2);

//	printf("5.5 / 2 = %f\n", 5.5 % 2);

	return 0;
}
